package com.spring.security.jwtbasic.jwtutils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.spring.security.jwtbasic.dao.UserDao;
import com.spring.security.jwtbasic.jwtutils.models.DAOUser;
import com.spring.security.jwtbasic.jwtutils.models.JwtRequestModel;
import com.spring.security.jwtbasic.jwtutils.models.JwtResponseModel;
import com.spring.security.jwtbasic.jwtutils.models.UserDTO;
@RestController
@CrossOrigin
public class JwtController {
   @Autowired
   private JwtUserDetailsService userDetailsService;
   @Autowired
   private AuthenticationManager authenticationManager;
   @Autowired
   private TokenManager tokenManager;
   
	@Autowired
	public UserDao userDao;
	
   @PostMapping("/login")
   public ResponseEntity<?> createToken(@RequestBody JwtRequestModel
   request) throws Exception {
      try {
         authenticationManager.authenticate(
            new
            UsernamePasswordAuthenticationToken(request.getUsername(),
            request.getPassword())
         );
      } catch (DisabledException e) {
         throw new Exception("USER_DISABLED", e);
      } catch (BadCredentialsException e) {
         throw new Exception("INVALID_CREDENTIALS", e);
      }
      final UserDetails userDetails = userDetailsService.loadUserByUsername(request.getUsername());
      final String jwtToken = tokenManager.generateJwtToken(userDetails);
      return ResponseEntity.ok(new JwtResponseModel(jwtToken));
   }
   
   @PostMapping("/register")
   public ResponseEntity<?> saveUser(@RequestBody UserDTO user) throws Exception {

	   Iterable<DAOUser> findAll = userDao.findAll();
	   int count=0;
	   for (DAOUser daoUser : findAll) {
		   if(daoUser.getUsername().equalsIgnoreCase(user.getUsername())) {
			count++;
		     }
	      }
	   if(count == 0) { 
		   return ResponseEntity.ok(userDetailsService.save(user));
		   }
	   else {
		   return ResponseEntity.ok("user already exist");
	   }
	}
}
