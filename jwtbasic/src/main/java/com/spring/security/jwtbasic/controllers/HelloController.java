package com.spring.security.jwtbasic.controllers; 
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.spring.security.jwtbasic.dao.UserDao;
import com.spring.security.jwtbasic.jwtutils.models.DAOUser;
import com.spring.security.jwtbasic.jwtutils.models.User;
import com.spring.security.jwtbasic.jwtutils.models.UserDTO; 

@CrossOrigin(origins = "http://localhost:4200")
@RestController 
public class HelloController {
	@Autowired
	public UserDao userDao;
	
   @GetMapping("/hello") 
   public String hello() { 
      return "hello guhan"; 
   } 
   
   @GetMapping(value="/loginDetails",produces = "application/json") 
   public Iterable<DAOUser> loginDetails() { 
      return userDao.findAll(); 
   } 
   
   @GetMapping(produces = "application/json")
	@RequestMapping({ "/validateLogin" })
	public User validateLogin() {
		return new User("User successfully authenticated");
	}
   
//   @GetMapping("/employees")
//   public List<UserDTO> firstPage() {
//	   
//	   
//	   return ;
//   }
   }

